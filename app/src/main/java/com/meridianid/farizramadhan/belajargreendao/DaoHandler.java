package com.meridianid.farizramadhan.belajargreendao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by fariz ramadhan.
 * website : www.farizdotid.com
 * github : https://github.com/farizdotid
 */


public class DaoHandler {
    public static DaoSession getInstance(Context context){
        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(context, "Belajar-Greendao-db", null);
        SQLiteDatabase db = helper.getWritableDatabase();

        DaoMaster daoMaster = new DaoMaster(db);
        return daoMaster.newSession();
    }
}