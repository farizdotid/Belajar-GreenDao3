package com.meridianid.farizramadhan.belajargreendao;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.btnAddData)
    Button btnAddData;
    @BindView(R.id.btnShowData)
    Button btnShowData;

    DaoSession daoSession;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        daoSession = DaoHandler.getInstance(this);
        btnAddData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addData();
            }
        });

        btnShowData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showData();
            }
        });
    }

    private void addData(){
        User user = new User();
        user.setName("ramadhan");
        daoSession.insert(user);

        Toast.makeText(this, "Add Data Success", Toast.LENGTH_SHORT).show();
    }

    private void showData(){
        String nama = daoSession.getUserDao().queryBuilder().list().get(1).getName();
        Toast.makeText(this, "Hallo " + nama, Toast.LENGTH_SHORT).show();
    }
}
